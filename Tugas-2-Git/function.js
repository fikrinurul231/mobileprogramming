//1.   
function teriak() {
    console.log("Halo Humanika!");
  }
  
  console.log(teriak()) // "Halo Humanika!" 
  console.log ('')

  //2.
  function kalikan(num1_param, num2_param) {
    return num1_param * num2_param;
  }
  
  var num1 = 12;
  var num2 = 4;
  
  var hasilPerkalian = kalikan(num1,num2);
  console.log(hasilPerkalian); // Menampilkan angka 48
  console.log('')

  //3. 
  function introduce(name_param, age_param, address_param, hobby_param) {
    console.log("Nama saya " + name_param + ", umur saya " + age_param + ", alamat saya di " + address_param + ", dan saya punya hobby yaitu " + hobby_param);
  }
  
  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogyakarta";
  var hobby = "gaming";
  
  var perkenalan = introduce(name,age,address,hobby);
  console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu gaming!"
  