import React, { useState } from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";

const Hitungs = () => {
    const [panjang, setPanjang] = useState("");
    const [lebar, setLebar] = useState("");
    const [tinggi, setTinggi] = useState("");
    const [volume, setVolume] = useState("");
    const [luas, setLuas] = useState("");

    const hitungvolume=(panjang, lebar, tinggi)=>{
      const temp = panjang*lebar*tinggi;
      setVolume(temp);
    }
    const hitungluas=(panjang, lebar, tinggi)=>{
      const temp = 2*((panjang*lebar)+(panjang*tinggi)+(lebar*tinggi))
      setLuas(temp);
    }
    return (
      <View style={styles.container}>
        <View style={styles.posTitle}>
          <Text style={styles.title}>Menghitung Balok by Fikri N.I</Text>
        </View>

          <View style={styles.contInput}>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan Panjang"
                  placeholderTextColor="#000"
                  onChangeText={(value)=>setPanjang(value)}
                  value={panjang}
                  keyboardType = 'numeric'
                />
            </View>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan Lebar"
                  placeholderTextColor="#000"
                  onChangeText={(value)=>setLebar(value)}
                  value={lebar}
                  keyboardType = 'numeric'
                />
            </View>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan Tinggi"
                  placeholderTextColor="#000"
                  onChangeText={(value)=>setTinggi(value)}
                  value={tinggi}
                  keyboardType = 'numeric'
                />
            </View>
            <View style={styles.posButton}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={()=>{hitungvolume(panjang, lebar, tinggi), hitungluas(panjang, lebar, tinggi)}}
                  >
                  <Text style={styles.textButton}>Hitung</Text>
                  </TouchableOpacity>
            </View>
          </View>
          
        <View style={styles.posOutput}>
          <Text>Volume Balok Nya Adalah </Text>
          <Text style={styles.textOutput}>{volume}</Text>
        </View>
        <View style={styles.posOutput}>
         <Text>Luas Balok Nya Adalah </Text>
          <Text style={styles.textOutput}>{luas}</Text>
      </View>
      </View>
    )
};
const styles = StyleSheet.create({
    container:{
      marginTop:120
    },
    posTitle:{
      alignItems: 'center'
    },
    title:{
      fontSize : 20,
      fontWeight : 'bold'
    },
    contInput:{
      backgroundColor: '#FF0000',
      margin: 20,
      padding: 20,
      borderRadius:40,
    },
    posInput:{
      marginLeft : 20,
      marginRight : 20,
      marginBottom : 10,
      backgroundColor : '#fff',
      paddingLeft : 10,
      paddingRight: 10,
      borderRadius: 20,
    },
    input:{
      height : 30
    },
    posButton:{
      margin: 20,
      alignItems:'center'
    },
    button:{
      borderRadius: 10,
      width: 180,
      height: 30,
      alignItems:'center',
      backgroundColor : '#fff',
      justifyContent : 'center'
    },
    textButton:{
      fontWeight: 'bold',
      color: '#000'
    },
    posOutput:{
      alignItems:'center'
    },
    textOutput:{
      fontSize: 30,
      fontWeight: 'bold'
    }
})

export default Hitungs;